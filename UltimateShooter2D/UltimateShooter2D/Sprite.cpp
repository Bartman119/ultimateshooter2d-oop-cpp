#include "Sprite.h"
#include "Graphics.h"
#include "SDL.h"
#include "Globals.h"
#include <iostream>

Sprite::Sprite()
{
}

Sprite::Sprite(Graphics & graphics, const std::string & filePath, int sourceX, int sourceY, int width, int height,
	float posX, float posY): _x(posX), _y(posY)
{
	this->_sourceRect.x = sourceX;
	this->_sourceRect.y = sourceY;
	this->_sourceRect.w = width;
	this->_sourceRect.h = height;

	this->_spriteSheet = SDL_CreateTextureFromSurface(graphics.getRenderer(), graphics.loadImage(filePath));
	if (this->_spriteSheet == NULL) 
	{
		std::cout << "\nError: unable to load an image!\n";
	}

	this->_boundingBox = Rectangle(this->_x, this->_y, std::max(int(width * globals::SPRITE_SCALE),1), std::max(int(height * globals::SPRITE_SCALE), 1));
}

Sprite::~Sprite()
{
}

void Sprite::update()
{
	//bounding box follows the player
	this->_boundingBox = Rectangle(this->_x, this->_y, this->_sourceRect.w * globals::SPRITE_SCALE, this->_sourceRect.h * globals::SPRITE_SCALE);
}

void Sprite::draw(Graphics & graphics, int x, int y)
{
	SDL_Rect destinationRectangle = { x, y, std::max(int(this->_sourceRect.w * globals::SPRITE_SCALE),1) /*scales sprites*/, std::max(int(this->_sourceRect.h * globals::SPRITE_SCALE),1) };
	graphics.blitSurface(this->_spriteSheet, &this->_sourceRect, &destinationRectangle);
}

const Rectangle Sprite::getBoundingBox() const
{
	return this->_boundingBox;
}

//Determine which side the collision happened on
const sides::Side Sprite::getCollisionSide(Rectangle & other) const
{
	//determine which side of sprite collides the most
	int amtRight, amtLeft, amtTop, amtBottom;
	//lowest amount (bc it's the difference) means the side of collision
	amtRight = this->getBoundingBox().getRight() - other.getLeft();
	amtLeft = other.getRight() - this->getBoundingBox().getLeft();
	amtTop = other.getBottom() - this->getBoundingBox().getTop();
	amtBottom = this->getBoundingBox().getBottom() - other.getTop();

	int vals[4] = { abs(amtRight), abs(amtLeft), abs(amtTop), abs(amtBottom) };

	int lowest = vals[0];
	for (int i = 0; i < 4; i++)
	{
		if (vals[i] < lowest)
			lowest = vals[i];
	}

	return 
		lowest == abs(amtRight) ? sides::RIGHT:
		lowest == abs(amtLeft) ? sides::LEFT:
		lowest == abs(amtTop) ? sides::TOP:
		lowest == abs(amtBottom) ? sides::BOTTOM:
		sides::NONE;
}


