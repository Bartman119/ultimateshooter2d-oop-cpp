#pragma once
#ifndef  HUDELEMENTS_H
#define HUDELEMENTS_H

#include <SDL.h>
#include <string>
#include <SDL_ttf.h>
#include "Graphics.h"

/**
class used to draw fonts and create hud, derived from graphics
*/
class HUDElements : public Graphics
{
private:
	const char* _font; /**< font directory */
	int _size; /**< font size */
	unsigned char _r, _g, _b; /**< font color */
public:
	const char * _text; /**< text to show */
	TTF_Font* fontType; /**< type of font */
	SDL_Surface* surfaceMessage; /**< message to show */
	SDL_Color color; /**< color based on rgb values */
	SDL_Texture* message;
	SDL_Rect messageRect;

	HUDElements();

	/**
	constructor for hud element
	@param graphics Graphics class member representing SDL graphical functionality
	@param font font directory
	@param size size font
	@param red red color byte value
	@param green green color byte value
	@param blue blue color byte value
	@param text text to display
	@param messageX x coordinate of message
	@param messageY y coordinate of message
	@param messageWidth width of text
	@param messageHeight height of text
	*/
	HUDElements(Graphics & graphics, const char* font, int size, unsigned char red, unsigned char green, unsigned char blue, const char* text, int messageX, int messageY, int messageWidth, int messageHeight);

	~HUDElements();

	/**
		function that adds value to string
		@param value addition string to add
	*/
	void addValue(const std::string & value);

	/**
	 create hud display
	 @param text text to display
	*/
	void createSurface(const char* text);

	/**
	draw on screen
	@param graphics @param graphics Graphics class member representing SDL graphical functionality
	*/
	void draw(Graphics & graphics);

};


#endif // ! HUDELEMENTS_H
