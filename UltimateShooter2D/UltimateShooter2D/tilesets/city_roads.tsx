<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="2019.12.11" name="Roads" tilewidth="32" tileheight="32" tilecount="475" columns="25">
 <image source="../images/Street.png" width="800" height="608"/>
 <tile id="11">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="86">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="87">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="224">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="226">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="249">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="274">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="299">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="311">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="312">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="320">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="321">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="324">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="345">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="346">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="349">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="363">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="368">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="369">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="370">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="371">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="374">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="386">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="387">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="393">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="394">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="395">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="396">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="399">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="423">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="424">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="437">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
