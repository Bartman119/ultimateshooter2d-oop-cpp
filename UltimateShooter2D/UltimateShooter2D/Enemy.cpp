#include "Enemy.h"

void Enemy::update(int elapsedTime, Player &player, float enemyPosX, float enemyPosY)
{
	
	this->_playerPosX = player.getX();
	this->_playerPosY = player.getY();
	float distance = this->calculateDistance(enemyPosX, enemyPosY);
	if (distance > 40 && distance < 60)
		return;


	float direction = atan2((_playerPosY - enemyPosY), (_playerPosX - enemyPosX));
	float dx = this->getSpeed() * cos(direction);
	float dy = this->getSpeed() * sin(direction);
		
	float absoluteDx = abs(dx);
	float absoluteDy = abs(dy);

	if (absoluteDx > absoluteDy) 
	{
		if (dx < 0) 
		{
			_direction = LEFT;
			this->playAnimation("RunLeft");
		}
		else 
		{
			_direction = RIGHT;
			this->playAnimation("RunRight");
		}
	}		
	else 
	{	
		if (dy < 0) 
		{
			_direction = UP;
			this->playAnimation("RunUp");
		}
		else 
		{
			_direction = DOWN;
			this->playAnimation("RunDown");
		}	
	}
	float plusX = 0;
	float plusY = 0;

	if (distance > 200) 
	{
		plusX = dx * elapsedTime;
		plusY = dy * elapsedTime;
	}
	else if (distance < 40) 
	{
		plusX = -(dx * elapsedTime);
		plusY = -(dy * elapsedTime);
	}
	else
	{
		if (_direction == LEFT)
			this->playAnimation("IdleLeft");
		else if (_direction == RIGHT)
			this->playAnimation("IdleRight");
		else if (_direction == UP)
			this->playAnimation("IdleUp");
		else if (_direction == DOWN)
			this->playAnimation("IdleDown");
	}
	this->_enemyPosX += plusX;
	this->_enemyPosY += plusY;
	AnimatedSprite::update(elapsedTime);
}

void Enemy::draw(Graphics & graphics)
{
	AnimatedSprite::draw(graphics, this->getPosX(), this->getPosY());
}


Enemy::Enemy(Graphics & graphics, float playerX,  float playerY, float enemyPosX, float enemyPosY, std::string filePath, int sourceX, int sourceY, int width, int height, Vector spawnPoint, int timeToUpdate):
	AnimatedSprite(graphics, filePath, sourceX, sourceY, width, height, spawnPoint.x, spawnPoint.y, timeToUpdate),
	_playerSpawnX(playerX),
	_playerSpawnY(playerY),
	_enemyPosX(enemyPosX),
	_enemyPosY(enemyPosY),
	_direction(DOWN),
	_maxHealth(0),
	_currentHealth(0)
{}

Enemy::~Enemy()
{
	//this->~Sprite();
	delete this;
}

void Enemy::animationDone(std::string currentAnimation)
{
}

void Enemy::setupAnimations()
{
}

void Enemy::handleTileCollisions(std::vector<Rectangle>& others)
{
	for (int i = 0; i < others.size(); i++)
	{
		sides::Side collisionSide = Sprite::getCollisionSide(others.at(i));
		if (collisionSide != sides::NONE)
		{
			switch (collisionSide)
			{
			case sides::TOP:
				//set top of sprite to bottom of object + 1 pixel
				this->_enemyPosY = others.at(i).getBottom() + 1;
				//this->_dy = 0;
				break;
			case sides::BOTTOM:
				this->_enemyPosY = others.at(i).getTop() - this->_boundingBox.getHeight() - 1;
				//this->_dy = 0;
				break;
			case sides::LEFT:
				this->_enemyPosX = others.at(i).getRight() + 1;
				break;
			case sides::RIGHT:
				this->_enemyPosX = others.at(i).getLeft() - this->_boundingBox.getWidth() - 1;
				break;
			}
		}
	}
}

void Enemy::handleBulletCollisions(std::vector<Rectangle>& others, Bullet* _bullet)
{
	//deal damage
	//int health = this->getCurrentHealth();
	this->setCurrentHealth(_bullet->getDamage());
	delete _bullet;
	//if killed delete
	if (this->getCurrentHealth() <= 0)
		this->~Enemy();
}

float Enemy::calculateDistance(float enemyPosX, float enemyPosY)
{
	float distance = sqrt(pow(_playerPosX - enemyPosX, 2) + pow(_playerPosY - enemyPosY, 2));
	return distance*globals::SPRITE_SCALE;
}

const float Enemy::getSpeed() //potential problem source
{
	return 0.0f;
}
