#pragma once
#ifndef TILE_H
#define TILE_H
#include <SDL.h>
#include "Globals.h"
#include "Graphics.h"

class Tile 
{
private:
	SDL_Texture * _tileset;
		//size of tile,	position on tileset, position on map
	
public:
	Vector _size, _tilesetPosition, _position;
	Tile();
	Tile(SDL_Texture * tileset, Vector size, Vector tilesetPosition, Vector position);
	void update(int elapsedTime);
	void draw(Graphics &graphics);
};

#endif // !TILE_H
