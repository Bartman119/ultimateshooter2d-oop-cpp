#pragma once
#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <map>
#include <string>
#include <SDL.h>

struct SDL_Window; //forward declaration of pointers
struct SDL_Renderer; //used to ommit excess inclusion of files

/**
	Graphics class
	used to visualize the game, holds all data of graphics in the game
*/
class Graphics {
private:
	SDL_Window* _window; /**< place where we draw */
	SDL_Renderer* _renderer; /**< does all the drawing */

	std::map<std::string, SDL_Surface*> _spriteSheets;	/**< holds all spritesheets loaded once to optimise the program return image either if it was loaded or not */

public:
	Graphics();
	~Graphics();

	/** loads an image into _spriteSheets map if it doesn't exist
	draws images to screen
	@param filePath path to the image to be loaded
	*/
	SDL_Surface* loadImage(const std::string &filePath);	
	
	/**
	function doing actual drawing on the screen
	@param source loaded texture source
	@param sourceRectangle coordinates of a rectangle to be drawn from spritesheet
	@param destinationRectangle coordinates of a rectangle part of a screen 
	*/
	void blitSurface(SDL_Texture* source, SDL_Rect* sourceRectangle, SDL_Rect* destinationRectangle);
	
	
	/**
	renders all images to screen
	*/
	void flip();

	/**
	clear renderer
	*/
	void clear();

	/**
	return renderer to use it elsewhere
	*/
	SDL_Renderer* getRenderer() const;
};

#endif // !GRAPHICS_H
