#pragma once
#ifndef  BULLET_H
#define BULLET_H

#include "Sprite.h"
#include "Graphics.h"
#include "Rectangle.h"
#include <vector>

/**
	Class representing bullet shot by creatures, derives directly from Sprite
*/
class Bullet : public Sprite
{
private:
	float _x; /**< xcoordinate of bullet */
	float _y; /**< ycoordinate of bullet */
	float _mousePosX; /**< xcoordinate of mouse cursor */
	float _mousePosY; /**< ycoordinate of mouse cursor */
	float _bulletSpeed; /**< speed of bullet */
	float _dx; /**< change of xcoordinate of bullet */
	float _dy; /**< change of ycoordinate of bullet */

public:
	float _bulletDamage; /**< damage value of bullet */
	
	/**
		default constructor being removed
	*/
	Bullet() = delete;
	
	/**
		Constructor passing parameters to Sprite and Bullet
		@param graphics Graphics class member representing SDL graphical functionality
		@param filePath path to spritesheet
		@param posX current X coordinate position of an object that is drawn in game
		@param posY current Y coordinate position of an object that is drawn in game
		@param mousePosX X position of mouse cursor
		@param mousePosY Y position of mouse cursor
		@param bulletSpeed speed of a bullet
		@param bulletDamage damage of a bullet
		@param sourceX starting X coordinate of sprite
		@param sourceY starting Y coordinate of sprite
		@param width pixel value of width of a single frame
		@param height pixel value of height of a single frame
	*/
	Bullet(Graphics &graphics, const std::string & filePath, float posX, float posY, float mousePosX, float mousePosY, float bulletSpeed, float bulletDamage, int sourceX, int sourceY, int width, int height);
	
	/**
		function updating class
		@param elapsedTime value representing time, used to unify time it takes to make operations on different PCs
	*/
	void update(int elapsedTime);
	
	/**
	function that draws bullet periodically
	@param graphics Graphics class member representing SDL graphical functionality
	*/
	void draw(Graphics &graphics);

	/**
	getter for damage of a bullet
	*/
	float getDamage() { return _bulletDamage; }
};

#endif // ! BULLET_H
