
#include "SDL.h"
#include "Graphics.h"
#include "SDL_image.h"
#include "Globals.h"



Graphics::Graphics()
{
	SDL_CreateWindowAndRenderer(globals::SCREEN_WIDTH, globals::SCREEN_HEIGHT, 0, &this->_window, &this->_renderer);
	SDL_SetWindowTitle(this->_window, "BloodSport");
}

Graphics::~Graphics()
{
	SDL_DestroyWindow(this->_window);
	SDL_DestroyRenderer(this->_renderer);
}

SDL_Surface * Graphics::loadImage(const std::string & filePath)
{
	if (this->_spriteSheets.count(filePath) == 0) //was it loaded yet
	{
		this->_spriteSheets[filePath] = IMG_Load(filePath.c_str());
	}
	return this->_spriteSheets[filePath];
}

void Graphics::blitSurface(SDL_Texture * source, SDL_Rect * sourceRectangle, SDL_Rect * destinationRectangle)
{
	SDL_RenderCopy(this->_renderer, source, sourceRectangle, destinationRectangle); //copies image to render to a certain renderer
}

void Graphics::flip()
{
	SDL_RenderPresent(this->_renderer);
}

void Graphics::clear()
{
	SDL_RenderClear(this->_renderer);
}

SDL_Renderer * Graphics::getRenderer() const
{
	return this->_renderer;
}


