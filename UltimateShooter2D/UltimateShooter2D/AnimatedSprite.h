#pragma once
#ifndef ANIMATED_SPRITE_H
#define ANIMATED_SPRITE_H

#include <map>
#include <string>
#include <vector>
#include "Graphics.h"
#include "Sprite.h"
#include "Globals.h"

/**	AnimatedSprite class
	contains all logic for animating sprites
*/
class AnimatedSprite : public Sprite
{
private:
	//holds all animations in a map
	
	std::map<std::string, std::vector<SDL_Rect>> _animations; /**< holds each frame of animation */
	
	std::map<std::string, Vector> _offsets;	/**< stores amount of pixels we want object to move */
	
	int _frameIndex; /**< index of a frame that is supposed to be drawn currently */
	double _timeElapsed; /**< value that represents time of drawing for each frame  */
	bool _isVisible; /**< flag that allows to make some sprites invisible */

protected:
	double _timeToUpdate; /**< value generalizing time it takes for a pc to make an operation */
	bool _currentAnimationOnce; /**< flag to play animation just once */
	std::string _currentAnimation; /**< name of current animation */

	/** void addAnimation
		function adding animation to map to call and use later
		@param numberOfFrames how many frames animation has
		@param x value of starting x coordinate for animation on a spritesheet 
		@param y value of starting y coordinate for animation on a spritesheet
		@param name string name of the file that holds the spritesheet
		@param width pixel value of width of a single frame
		@param height pixel value of height of a single frame
		@param offset pair of coordinates for optional offset for central point of a sprite
	*/
	void addAnimation(int numberOfFrames, int x, int y, std::string name, int width, int height, Vector offset);

	/**
		function that resets current animation
	*/
	void resetAnimations();

	/**
		function that pauses current animation
	*/
	void stopAnimation();

	/**
		changes visibility of animated sprite
		@param isVisible flag for visibility of sprite 
	*/
	void setVisible(bool isVisible);
	
	/**
		Logic executed after animation is done
		@param currentAnimation string name of a current animation
	*/
	virtual void animationDone(std::string currentAnimation) = 0;

	/** 
		functions needed to setup animations
	*/
	virtual void setupAnimations() = 0;

public:
	/**
	default constructor for AnimatedSprite
	*/
	AnimatedSprite();

	/**
	Constructor that passes value to Sprite derived class and sets up basic information about the spritesheet
	@param graphics Graphics class member representing SDL graphical functionality
	@param filePath path to spritesheet
	@param sourceX starting X coordinate of an animation
	@param sourceY starting Y coordinate of an animation
	@param width pixel value of width of a single frame
	@param height pixel value of height of a single frame
	@param posX current X coordinate position of an object that is drawn in game
	@param posY current Y coordinate position of an object that is drawn in game
	@param timeToUpdate value generalizing time it takes for a pc to make an operation
	*/
	AnimatedSprite(Graphics & graphics, const std::string & filePath, int sourceX, int sourceY, int width, int height,
		float posX, float posY, float timeToUpdate); //time for each animation frame
	/**
		uses animation's given name to play it if it already doesn't,
		@param animation name of animation
		@param once flag checking if animation should be played repeatedly
	*/
	void playAnimation(std::string animation, bool once = false);

	/**
		function updating class 
		@param elapsedTime value representing time, used to unify time it takes to make operations on different PCs
	*/
	void update(int elapsedTime);
	
	/**
	function that draws animations periodically
	@param graphics Graphics class member representing SDL graphical functionality
	@param x Xcoordinate of an object
	@param y Ycoordinate of an object
	*/
	void draw(Graphics &graphics, int x, int y);

	
};
#endif // !ANIMATED_SPRITE_H
