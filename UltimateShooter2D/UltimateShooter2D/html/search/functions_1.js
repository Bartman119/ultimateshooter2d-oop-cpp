var searchData=
[
  ['beginnewframe',['beginNewFrame',['../class_input.html#ac82c7c0bdc0b672b8d1354303ad09235',1,'Input']]],
  ['blitsurface',['blitSurface',['../class_graphics.html#a642f22abc71149fc09cf7829ed8eeb50',1,'Graphics']]],
  ['boolattribute',['BoolAttribute',['../classtinyxml2_1_1_x_m_l_element.html#a53eda26131e1ad1031ef8ec8adb51bd8',1,'tinyxml2::XMLElement']]],
  ['booltext',['BoolText',['../classtinyxml2_1_1_x_m_l_element.html#a68569f59f6382bcea7f5013ec59736d2',1,'tinyxml2::XMLElement']]],
  ['boolvalue',['BoolValue',['../classtinyxml2_1_1_x_m_l_attribute.html#a98ce5207344ad33a265b0422addae1ff',1,'tinyxml2::XMLAttribute']]],
  ['bullet',['Bullet',['../class_bullet.html#a5e8fd8844271779a2e6cf28a72b54da9',1,'Bullet::Bullet()=delete'],['../class_bullet.html#a32e89e95d3c712cdf9de0f3da1cc771e',1,'Bullet::Bullet(Graphics &amp;graphics, const std::string &amp;filePath, float posX, float posY, float mousePosX, float mousePosY, float bulletSpeed, float bulletDamage, int sourceX, int sourceY, int width, int height)']]]
];
