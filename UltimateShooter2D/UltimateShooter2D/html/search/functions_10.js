var searchData=
[
  ['readbom',['ReadBOM',['../classtinyxml2_1_1_x_m_l_util.html#ae9bcb2bc3cd6475fdc644c8c17790555',1,'tinyxml2::XMLUtil']]],
  ['rectangle',['Rectangle',['../class_rectangle.html#a8a933e0ebd9e80ce91e61ffe87fd577e',1,'Rectangle::Rectangle()'],['../class_rectangle.html#a6fd48c1264965fd841b2d35b7736a352',1,'Rectangle::Rectangle(int x, int y, int width, int height)']]],
  ['reset',['Reset',['../classtinyxml2_1_1_str_pair.html#a80c1b3bd99bf62ae85c94a29ce537125',1,'tinyxml2::StrPair']]],
  ['resetanimations',['resetAnimations',['../class_animated_sprite.html#a7118dacbdfdf6dcbe7343d673ff307b4',1,'AnimatedSprite']]],
  ['rootelement',['RootElement',['../classtinyxml2_1_1_x_m_l_document.html#ad2b70320d3c2a071c2f36928edff3e1c',1,'tinyxml2::XMLDocument::RootElement()'],['../classtinyxml2_1_1_x_m_l_document.html#a2be8ef9d6346bdef34311f91529afae4',1,'tinyxml2::XMLDocument::RootElement() const']]]
];
