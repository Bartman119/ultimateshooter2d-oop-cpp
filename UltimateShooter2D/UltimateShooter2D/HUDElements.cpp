#include "HUDElements.h"
#include <iostream>
HUDElements::HUDElements()
{
}

HUDElements::HUDElements(Graphics & graphics, const char* font, int size, unsigned char red, unsigned char green, unsigned char blue, const char* text, int messageX, int messageY, int messageWidth, int messageHeight):
	_font(font), _size(size), _r(red), _g(green), _b(blue), _text(text)
{
	this->messageRect.x = messageX;
	this->messageRect.y = messageY;
	this->messageRect.h = messageHeight;
	this->messageRect.w = messageWidth;

	fontType = TTF_OpenFont(_font, _size);
	if(fontType == NULL)
		std::cerr << "TTF_OpenFont() Failed: " << TTF_GetError() << std::endl;
	color = { _r,_g,_b };
	createSurface(text);
	message = SDL_CreateTextureFromSurface(graphics.getRenderer(), surfaceMessage);

}

HUDElements::~HUDElements()
{
}

void HUDElements::addValue(const std::string & value)
{
	this->_text + value;
}



void HUDElements::createSurface(const char * text)
{
	surfaceMessage = TTF_RenderText_Solid(fontType, text, color);
}

void HUDElements::draw(Graphics & graphics)
{
	graphics.blitSurface(message, NULL, &messageRect);
	//SDL_RenderCopy(this->getRenderer(), message, NULL, &messageRect);
}
