#pragma once
#ifndef LEVEL_H
#define LEVEL_H
#include <string>
//#include "Globals.h"
#include <vector>
#include "Graphics.h"
#include "Tile.h"
#include "Rectangle.h"
#include "Player.h"
#include "Enemy.h"
#include "Armor1.h"

struct SDL_Texture; //forward-declaration
struct SDL_Rect;
struct Tileset;


/**
structure storing all level data, parsing xml file
*/
class Level 
{
private:
	std::string _mapName; /**< name of the map */
	Vector _spawnPoint; /**< point of spawn for player */
	Vector _size; /**< map size */
	Vector _tileSize; /**< tile size */

	SDL_Texture* _backgroundTexture; //unnecessary

	/**
	vector storing data of all tile that will be drawn to display
	*/
	std::vector<Tile> _tileList;  
	
	/**
	vector storing data of available tilesets
	*/
	std::vector<Tileset> _tilesets;
	
	/**
	vector storing data of all tiles that collide with each other
	*/
	std::vector<Rectangle> _collisionRects;

	

	

	/** 
		loads a map
		@param mapName name of the map
		@param graphics Graphics class member representing SDL graphical functionality
	*/
	void loadMap(std::string mapName, Graphics &graphics);
public:
	Level();

	/**
	constructor that gets information about map to load
	@param mapName name of the map
	@param spawnPoint point of the spawn for player
	@param graphics Graphics class member representing SDL graphical functionality
	*/
	Level(std::string mapName, Vector spawnPoint, Graphics &graphics);
	~Level();

	/**
	function updating all elements of a level
	@param elapsedTime time for inufying all pc performance times
	@param player PLayer class for parameters
	*/
	void update( int elapsedTime, Player &player);

	/**
	function drawing map
	@param
	graphics Graphics class member representing SDL graphical functionality
	*/
	void draw(Graphics &graphics);

	/**
	check collisions of something with map tiles
	@param other all tiles to check
	*/
	std::vector<Rectangle> checkTileCollisions(const Rectangle &other);

	/**
	getter for player spawnpoint
	*/
	const Vector getPlayerSpawnPoint() const;

	/**
	vector of enemies using polymorphism to get method for type of enemy we want
	*/
	std::vector<Enemy*> _enemies;

	/**
	vector of possible spawnPoints coordinates
	*/
	std::vector<Vector> _enemySpawnPoints;
};

/**
tileset structure
*/
struct Tileset 
{
	SDL_Texture * Texture; //tileset image
	int FirstGid;

	Tileset() 
	{
		this->FirstGid = -1;
	}

	Tileset(SDL_Texture * texture, int firstGid)
	{
		this->Texture = texture;
		this->FirstGid = firstGid;
	}
};

#endif // !LEVEL_H


