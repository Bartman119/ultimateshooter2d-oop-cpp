#pragma once
#ifndef SPRITE_H
#define SPRITE_H
#include <SDL.h>
#include "Graphics.h"
#include "Rectangle.h"
#include "Globals.h"
#include <algorithm>
/*
	Sprite class 
	Holds all info for sprites
*/


class Sprite {
private:
	
protected:
	//coordinates of a rectangle that show what piece of spriteSheet to draw
	SDL_Rect _sourceRect;
	//spriteshee location
	SDL_Texture* _spriteSheet;
	float _x, _y;
	//box bounding our sprite, used for collision
	Rectangle _boundingBox;

public:
	Sprite();
	Sprite(Graphics &graphics, const std::string & filePath, int sourceX, int sourceY, int width, int height, float posX, float posY);
	virtual ~Sprite();
	virtual void update();
	void draw(Graphics &graphics, int x, int y);

	const Rectangle getBoundingBox() const;
	const sides::Side getCollisionSide(Rectangle &other) const;

};
#endif // SPRITE_H
