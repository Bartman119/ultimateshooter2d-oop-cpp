#pragma once
/*
	Input class
	Knows everything about keyboard/mouse(in the future) states
*/
#ifndef INPUT_H
#define INPUT_H

#include "SDL.h"
#include <map>


/**
class used to detect input
*/
class Input {
public:
	void beginNewFrame(); /**< begin new frame for input */
	void keyPressedEvent(SDL_Event &event); /**< event tracking held key */
	void keyReleasedEvent(SDL_Event &event); /**< event tracking released key */
	bool wasKeyPressed(SDL_Scancode key); /**< event tracking whether key was pressed */
	bool wasKeyReleased(SDL_Scancode key); /**< event tracking whether key was released */
	bool isKeyHeld(SDL_Scancode key); /**< event tracking whether key is held*/

private:
	//add mouse handler
	std::map<SDL_Scancode, bool> _heldKeys;
	std::map<SDL_Scancode, bool> _pressedKeys;
	std::map<SDL_Scancode, bool> _releasedKeys;

};


#endif // !INPUT_H



