#include "Bullet.h"

Bullet::Bullet(Graphics &graphics, const std::string & filePath, float posX, float posY, float mousePosX, float mousePosY, float bulletSpeed, float bulletDamage, int sourceX, int sourceY, int width, int height):
	Sprite(graphics, filePath, sourceX,  sourceY, width, height, posX, posY),
	_x(posX), _y(posY), _mousePosX(mousePosX), _mousePosY(mousePosY),_bulletDamage(bulletDamage), _bulletSpeed(bulletSpeed)
{
	float direction = atan2((_mousePosY - _y), (_mousePosX - _x));
	_dx = _bulletSpeed * cos(direction);
	_dy = _bulletSpeed * sin(direction);
}


void Bullet::update(int elapsedTime)
{
	float xTraveled = _dx * elapsedTime;
	float yTraveled = _dy * elapsedTime;
	_x += xTraveled;
	_y += yTraveled;
}

void Bullet::draw(Graphics &graphics)
{
	Sprite::draw(graphics, this->_x, this->_y);
}
