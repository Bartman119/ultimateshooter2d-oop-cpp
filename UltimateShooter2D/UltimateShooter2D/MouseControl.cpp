#include "MouseControl.h"
MouseControl::MouseControl()
{
	mPosition.x = 0;
	mPosition.y = 0;
}

void MouseControl::HandleEvent(SDL_Event * event, Graphics &graphics, const std::string & filePath, float posX, float posY, float bulletSpeed, float bulletDamage, int sourceX, int sourceY, int width, int height)
{
	//Get mouse position
	int x, y;
	SDL_GetMouseState(&x, &y);
	//create a bullet
	//issues with push back, not visible to level 
	this->_bullets.push_back(new Bullet(graphics, filePath,posX, posY, x, y, bulletSpeed, bulletDamage, sourceX, sourceY, width, height));
	
}

void MouseControl::setPosition(int x, int y)
{
	mPosition.x = x;
	mPosition.y = y;
}

std::vector<Rectangle> MouseControl::checkBulletCollisions(const Rectangle & other)
{
	std::vector<Rectangle> others;
	for (int i = 0; i < this->_bullets.size(); i++)
	{
		if (this->_bullets.at(i)->getBoundingBox().collidesWith(other)) {
			others.push_back(this->_bullets.at(i)->getBoundingBox());
		}
	}
	return others;
}