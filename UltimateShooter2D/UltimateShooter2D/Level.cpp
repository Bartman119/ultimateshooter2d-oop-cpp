#include "Level.h"
#include "Graphics.h"
#include "Globals.h"
#include "Player.h"
#include <SDL.h>
#include "tinyxml2.h"
#include <sstream>
#include <algorithm>
#include <cmath>
#include <string>

using namespace tinyxml2;

Level::Level()
{
}

Level::Level(std::string mapName, Vector spawnPoint, Graphics & graphics) :
	_mapName(mapName), _spawnPoint(spawnPoint), _size(Vector(0,0))
{
	this->loadMap(mapName, graphics);
}

Level::~Level()
{
}

void Level::update(int elapsedTime, Player &player)
{
	for (int i = 0; i < this->_enemies.size(); i++) 
	{
		this->_enemies.at(i)->update(elapsedTime, player,this->_enemies.at(i)->getPosX(), this->_enemies.at(i)->getPosY());
	}

	//if you want to comment this code remember to uncomment player move
	//add if player is colliding then don't move map
	//move map based on movement??
	std::vector<Rectangle> others2;
	if ((others2 = this->checkTileCollisions(player.getBoundingBox())).size() == 0)
	{ 
		//while moving map, it re-draws map properly only on right and bottom side,
		//top and left are being squeezed
		/*for (int i = 0; i < this->_tileList.size(); i++)
		{
			if (player._dx < 0)
				this->_tileList.at(i)._position.x += (abs(player._dx) * elapsedTime);
			else if (player._dx > 0)
				this->_tileList.at(i)._position.x -= (abs(player._dx) * elapsedTime);
			else {}

			if (player._dy < 0)
				this->_tileList.at(i)._position.y += (abs(player._dy) * elapsedTime);
			else if (player._dy > 0)
				this->_tileList.at(i)._position.y -= (abs(player._dy) * elapsedTime);
			else {}
		}*/
		//update collision object position?
		/*for (int i = 0; i < this->_collisionRects.size(); i++)
		{
			if (player._dx < 0)
				this->_collisionRects.at(i)._x += (abs(player._dx) * elapsedTime);
			else if (player._dx > 0)
				this->_collisionRects.at(i)._x -= (abs(player._dx) * elapsedTime);

			if (player._dy < 0)
				this->_collisionRects.at(i)._y += (abs(player._dy) * elapsedTime);
			else if (player._dy > 0)
				this->_collisionRects.at(i)._y -= (abs(player._dy) * elapsedTime);
		}*/
	}
		
}

void Level::draw(Graphics & graphics)
{
	//draw the background //UNUSED
	/*SDL_Rect sourceRect = { 0, 0, 64, 64 };
	SDL_Rect destRect;
	for (int x = 0; x < this->_size.x / 64; x++) 
	{
		for (int y = 0; y < this->_size.y / 64; y++) 
		{
			destRect.x = x * 64 * globals::SPRITE_SCALE;
			destRect.y = y * 64 * globals::SPRITE_SCALE;
			destRect.w = 64 * globals::SPRITE_SCALE;
			destRect.h = 64 * globals::SPRITE_SCALE;
			graphics.blitSurface(this->_backgroundTexture, &sourceRect, &destRect);
		}
	}*/

	for (int i = 0; i < this->_tileList.size(); i++) 
	{
		this->_tileList.at(i).draw(graphics);
	}
	for (int i = 0; i < this->_enemies.size(); i++) 
	{
		this->_enemies.at(i)->draw(graphics);
	}

}

std::vector<Rectangle> Level::checkTileCollisions(const Rectangle & other)
{
	std::vector<Rectangle> others;
	for (int i = 0; i < this->_collisionRects.size(); i++) 
	{
		if (this->_collisionRects.at(i).collidesWith(other)) {
			others.push_back(this->_collisionRects.at(i));
		}
	}
	return others;
}

const Vector Level::getPlayerSpawnPoint() const
{
	return this->_spawnPoint;
}

void Level::loadMap(std::string mapName, Graphics & graphics)
{
	////TEMPORARY CODE< WILL CHANGE LATER
	//this->_backgroundTexture = SDL_CreateTextureFromSurface(graphics.getRenderer(),graphics.loadImage("backgrounds/Wall.png"));
	//this->_size = Vector(2560, 1440);

	//Parse the .tmx file
	XMLDocument doc;
	std::stringstream ss;
	ss << "maps/" << mapName << ".tmx";
	doc.LoadFile(ss.str().c_str());

	XMLElement* mapNode = doc.FirstChildElement("map");
	
	//get the width and the height of the whole map and store it in _size
	int width, height;
	mapNode->QueryIntAttribute("width", &width);
	mapNode->QueryIntAttribute("height", &height);
	this->_size = Vector(width, height);

	//get the width and height of a single tile and store it in _tileSize
	int tileWidth, tileHeight;
	mapNode->QueryIntAttribute("tilewidth", &tileWidth);
	mapNode->QueryIntAttribute("tileheight", &tileHeight);
	this->_tileSize = Vector(tileWidth, tileHeight);

	//Load the tilesets
	XMLElement *pTileset = mapNode->FirstChildElement("tileset");
	if (pTileset != NULL) 
	{
		XMLDocument doc2;
		std::stringstream ss2;
		while (pTileset) 
		{
			int firstgid;
			const char* source = pTileset->Attribute("source");  
			char* path;											

			ss2 << source;
			std::stringstream ss3;
			char ch;
			int counter = 0;

			//it just works, overengineered but works
			while(!ss2.eof())
			{
				ss2 >> ch;
				if (counter >= 3 && !ss2.eof()) 
				{
					ss3 << ch;
				}
				counter++;
			}
			ss2.clear();

			doc2.LoadFile(ss3.str().c_str());
			XMLElement *mapNode2 = doc2.FirstChildElement("tileset");
			std::stringstream ss4;
			if (mapNode2 != NULL) 
			{
				XMLElement *pMap = mapNode2->FirstChildElement("image");
				const char* source2 = pMap->Attribute("source");
				ss.str("");
				ss << source2;
				counter = 0;
				while (!ss.eof())
				{
					ss >> ch;
					if (counter >= 3 && !ss.eof())
					{
						ss4 << ch;
					}
					counter++;
				}
				ss.clear();
			}
			
			

			pTileset->QueryIntAttribute("firstgid", &firstgid);
			SDL_Texture* tex = SDL_CreateTextureFromSurface(graphics.getRenderer(), graphics.loadImage(ss4.str()));
			this->_tilesets.push_back(Tileset(tex, firstgid));

			pTileset = pTileset->NextSiblingElement("tileset");
		}
	}

	//loading the layers
	XMLElement* pLayer = mapNode->FirstChildElement("layer");
	if (pLayer != NULL) 
	{
		while (pLayer) {
			//Loading the data element
			XMLElement* pData = pLayer->FirstChildElement("data");
			if (pData != NULL) {
				while (pData) {
					//Loading the tile element
					XMLElement* pTile = pData->FirstChildElement("tile");
					if (pTile != NULL) {
						int tileCounter = 0;
						while (pTile) {
							//Build each individual tile here
							//If gid is 0, no tile should be drawn. Continue loop
							if (pTile->IntAttribute("gid") == 0) {
								tileCounter++;
								if (pTile->NextSiblingElement("tile")) {
									pTile = pTile->NextSiblingElement("tile");
									continue;
								}
								else {
									break;
								}
							}

							//Get the tileset for this specific gid
							int gid = pTile->IntAttribute("gid");
							Tileset tls;
							for(int i = 0; i < this->_tilesets.size(); i++)
							{
								if (i < this->_tilesets.size() - 1) //doesn't change anything?
								{
									if (this->_tilesets[i].FirstGid <= gid && this->_tilesets[i + 1].FirstGid > gid)
									{
										//that's the tileset we want
										tls = this->_tilesets.at(i);
										break;
									}
								}
								else 
								{
									tls = this->_tilesets.at(i);
									break;
								}
							}

							if (tls.FirstGid == -1) {
								//No tileset was found for this gid
								tileCounter++;
								if (pTile->NextSiblingElement("tile")) {
									pTile = pTile->NextSiblingElement("tile");
									continue;
								}
								else {
									break;
								}
							}

							//Calculate the position of the tile in the tileset
							//get the position of the tile in the level
							int xx = 0;
							int yy = 0;
							//coordinate based on tileCounter, moves xx position based on that
							xx = tileCounter % width;
							xx *= tileWidth;
							yy += tileHeight * (tileCounter / width);
							Vector finalTilePosition = Vector(xx, yy);


							int trueGid = gid - tls.FirstGid;


							//calculate the position of the tile in the tileset
							int tilesetWidth, tilesetHeight;
							SDL_QueryTexture(tls.Texture, NULL, NULL, &tilesetWidth, &tilesetHeight); 
							int tsxx = (trueGid % (tilesetWidth / tileWidth))/* - 1*/; //there's a problem with an algorithm i think
							tsxx *= tileWidth;

							//correction for the first row that gets negative values
							int checkValue = (tilesetWidth / tileWidth) - 1;
							tsxx = (tsxx < 0) ? checkValue * tileWidth : tsxx;

							int tsyy = 0;
							int amount = (trueGid / (tilesetWidth / tileWidth));
							tsyy = tileHeight * amount;

							Vector finalTilesetPosition = Vector(tsxx, tsyy);
							Tile tile(tls.Texture, Vector(tileWidth, tileHeight), finalTilesetPosition, finalTilePosition);	
							this->_tileList.push_back(tile);
							tileCounter++;
							pTile = pTile->NextSiblingElement("tile");
						}
					}
					pData = pData->NextSiblingElement("data");
				}
			}
			pLayer = pLayer->NextSiblingElement("layer");
		}
	}

	//Parse out collisions
	XMLElement* pObjectGroup = mapNode->FirstChildElement("objectgroup");
	
	//move to last element in SLL
	//while (pObjectGroup->NextSiblingElement("objectgroup")) //will have to change that later for spawn point location data
	//	pObjectGroup = pObjectGroup->NextSiblingElement("objectgroup");

	float playerX, playerY;
	if (pObjectGroup != NULL)
	{
		while (pObjectGroup)
		{
			const char * name = pObjectGroup->Attribute("name");
			std::stringstream ss; //might be a problem
			ss << name;
			

			if (ss.str() == "collisions")
			{
				XMLElement* pObject = pObjectGroup->FirstChildElement("object");
				if (pObject != NULL) 
				{
					while(pObject)
					{
						float x, y, width, height;
						x = pObject->FloatAttribute("x");
						y = pObject->FloatAttribute("y");
						width = pObject->FloatAttribute("width");
						height = pObject->FloatAttribute("height");
						this->_collisionRects.push_back(Rectangle(
							std::ceil(x) * globals::SPRITE_SCALE,
							std::ceil(y) * globals::SPRITE_SCALE,
							std::ceil(width) * globals::SPRITE_SCALE,
							std::ceil(height) * globals::SPRITE_SCALE));

						pObject = pObject->NextSiblingElement("object");
					}
				}
			
			}
			else if (ss.str() == "playerSpawn") 
			{
				XMLElement* pObject = pObjectGroup->FirstChildElement("object");
				if (pObject != NULL) 
				{
					while (pObject) 
					{
						float x = pObject->FloatAttribute("x");
						float y = pObject->FloatAttribute("y");
						playerX = x;
						playerY = y;
						this->_spawnPoint = Vector(std::ceil(x) * globals::SPRITE_SCALE, std::ceil(y) * globals::SPRITE_SCALE);
						pObject = pObject->NextSiblingElement("object");
					}
				}
			}
			else if (ss.str() == "enemySpawn") 
			{
				XMLElement *pObject = pObjectGroup->FirstChildElement("object");
				if (pObject != NULL) 
				{
					while (pObject) 
					{
						float x = pObject->FloatAttribute("x"); //does not save coordinates properly
						float y = pObject->FloatAttribute("y");
						//create a vector of spawn points to randomly pick from
						this->_enemySpawnPoints.push_back(Vector(std::floor(x) * globals::SPRITE_SCALE, std::floor(y) * globals::SPRITE_SCALE));

						/*Armor1 * enemyArmor = new Armor1(graphics, playerX, playerY, Vector(std::floor(x) * globals::SPRITE_SCALE, std::floor(y) * globals::SPRITE_SCALE));
						this->_enemies.push_back(enemyArmor);*/
						//this->_enemies.push_back(new Armor1(graphics, playerX, playerY, Vector(std::floor(x) * globals::SPRITE_SCALE, std::floor(y) * globals::SPRITE_SCALE)));

						pObject = pObject->NextSiblingElement("object");
					}
						
						
				}
			}
			pObjectGroup = pObjectGroup->NextSiblingElement("objectgroup");
		}
	}
}
