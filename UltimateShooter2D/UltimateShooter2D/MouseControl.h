#pragma once
#include <SDL.h>
#include "Graphics.h"
#include "Bullet.h"
#include <vector>
/**
class holding mouse input
*/
class MouseControl 
{
private:
	/**
	Top left position of the cursor
	*/
	SDL_Point mPosition;
public:
	std::vector<Bullet*> _bullets; /**< vector of all held bullets*/
	//Initializes internal variables
	MouseControl();
	
	/**
	 function chceking bullet collision with objcets
	 @param other object that can collide
	*/
	std::vector<Rectangle> checkBulletCollisions(const Rectangle & other);
	
	/**
	handler for mouse click
	*/
	void HandleEvent(SDL_Event * event, Graphics &graphics, const std::string & filePath, float posX, float posY, float bulletSpeed, float bulletDamage, int sourceX, int sourceY, int width, int height);

	/**
	Sets top left position setter
	*/
	void setPosition(int x, int y);

};