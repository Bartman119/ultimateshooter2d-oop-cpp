#include "Armor1.h"



Armor1::Armor1(Graphics & graphics, float playerX, float playerY, Vector spawnPoint) : posX(spawnPoint.x), posY(spawnPoint.y), Enemy(graphics, playerX, playerY, spawnPoint.x, spawnPoint.y, "assets/sprites/Armor1/ArmorWalk.png", 0, 0, 40, 40, spawnPoint, 140)
{
	//after AnimatedSprite and Sprite are called, here the data of those classes disappears
	this->setupAnimations();
	this->playAnimation("IdleDown");
}

void Armor1::update(int elapsedTime, Player & player)
{
	Enemy::update(elapsedTime, player, getPosX(), getPosY());
}

void Armor1::draw(Graphics & graphics)
{
	Enemy::draw(graphics);
}

void Armor1::animationDone(std::string currentAnimation)
{
}

void Armor1::setupAnimations()
{
	this->addAnimation(8, 1, 0, "RunUp", 64, 64, Vector());
	this->addAnimation(8, 1, 64, "RunLeft", 64, 64, Vector());
	this->addAnimation(8, 1, 128, "RunDown", 64, 64, Vector());
	this->addAnimation(8, 1, 192, "RunRight", 64, 64, Vector());
	this->addAnimation(1, 0, 0, "IdleUp", 64, 64, Vector());
	this->addAnimation(1, 0, 64, "IdleLeft", 64, 64, Vector());
	this->addAnimation(1, 0, 128, "IdleDown", 64, 64, Vector());
	this->addAnimation(1, 0, 192, "IdleRight", 64, 64, Vector());
}



