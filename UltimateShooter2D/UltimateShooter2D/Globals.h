#pragma once
#ifndef GLOBALS_H
#define GLOBALS_H

/**
global namespace holding all global variables
*/
namespace globals	//used as an additional measurement of security 
{					//when dealing with globals
	const int SCREEN_WIDTH = 1080;
	const int SCREEN_HEIGHT = 1080;

	const float SPRITE_SCALE = 0.75f;
	/*bool isMovingRight = false;
	bool isMovingLeft = false;
	bool isMovingUp = false;
	bool isMovingDown = false;*/
}

/**
namespace representing side of a sprite/level chunk
*/
namespace sides 
{
	enum Side 
	{
		TOP,
		BOTTOM,
		LEFT,
		RIGHT,
		NONE
	};

	inline Side getOppositeSide(Side side)
	{
		return
			side == TOP ? BOTTOM :
			side == BOTTOM ? TOP :
			side == LEFT ? RIGHT :
			side == RIGHT ? LEFT :
			NONE;
	}
}


/**
enum that represents directions of sprite's movement
*/
enum Direction {
	LEFT,
	RIGHT,
	UP,
	DOWN
};

/**
structure that holds positional data of an object, initializes it with (0,0) coordinates
*/
struct Vector {
	int x, y;
	Vector(): x(0),y(0){}

	/**
	Constructor passing pair of coordinates
	@param x x coordinate
	@param y y coordinate
	*/
	Vector(int x, int y): x(x), y(y){}

	//function returning default coordinates
	Vector zero() 
	{
		return Vector();
	}
};
#endif // !GLOBALS_H
