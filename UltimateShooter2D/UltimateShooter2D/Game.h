#pragma once
#ifndef GAME_H
#define GAME_H
#include "Player.h"
#include "Level.h"
#include "HUDElements.h"
#include "MouseControl.h"
#include "Bullet.h"
#include "Globals.h"
/*
	Game class
	holds all information of our game
*/
struct Graphics;

/**
class working as a game loop, stores all information about the game
*/
class Game {
private:
	void gameLoop(); /**< function where whole game loop occurs */
	void draw(Graphics &graphics); /**< function which draws all objects to the window */
	void update(float elapsedTime); /**< function updating all elements used by the game */

	Player _player;

	Level _level;
	MouseControl _mouse;
	HUDElements _hudHealth;
	HUDElements _hudScore;
	
	//std::vector<Bullet*> _bullets;

public:
	Game();
	~Game();
};


#endif // !GAME_H
