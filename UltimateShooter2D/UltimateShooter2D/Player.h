#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include "Graphics.h" //could forward-define
#include "AnimatedSprite.h"
#include "Globals.h"

class Player : public AnimatedSprite {
private:
	
	float _spawnPointX, _spawnPointY;
	Direction _facing;
public:
	int points;
	float _dx, _dy;
	Player();
	Player(Graphics &graphics, Vector spawnPoint);

	void draw(Graphics &graphics);
	void update(float elapsedTime);
	
	virtual void animationDone(std::string currentAnimation);
	virtual void setupAnimations();

	void handleTileCollisions(std::vector<Rectangle> &others);

	/*	void moveLeft
		moves the player left by -dx
	*/
	void moveLeft(bool & isMovingRight, bool & isMovingLeft, bool & isMovingUp, bool & isMovingDown);

	/*	void moveRight
		moves the player right by dx
	*/
	void moveRight(bool & isMovingRight, bool & isMovingLeft, bool & isMovingUp, bool & isMovingDown);

	/*	void moveUp
		moves the player up by -dy
	*/
	void moveUp(bool & isMovingRight, bool & isMovingLeft, bool & isMovingUp, bool & isMovingDown);

	/*	void moveDown
		moves the player down by dy
	*/
	void moveDown(bool & isMovingRight, bool & isMovingLeft, bool & isMovingUp, bool & isMovingDown);
	
	/*	void stopMoving
		stops player movement and resets Direction
	*/
	void stopMoving();

	const float getX() const;
	const float getY() const;

	void moveSprite();
	
};


#endif // !PLAYER_H
