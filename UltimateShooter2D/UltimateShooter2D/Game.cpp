#include "SDL.h"
#include "Graphics.h"
#include "SDL.h"
#include "Game.h"
#include "Input.h"
#include "Armor1.h"
#include "Enemy.h"
#include "MouseControl.h"
#include <algorithm>
#include <iostream>
#include <SDL_ttf.h>



// Constants
const int FPS = 50;
const int MAX_FRAME_TIME = 5 * 1000 / FPS; //maximal time allowed for a frame to execute 
bool isMovingRight = false;
bool isMovingLeft = false;
bool isMovingUp = false;
bool isMovingDown = false;
//shouldn't this be 20 ms??
Game::Game()
{
	SDL_Init(SDL_INIT_EVERYTHING); //initialize all modules in sdl
	TTF_Init();
	this->gameLoop();
}

Game::~Game()
{
}

int LAST_UPDATE_TIME = SDL_GetTicks(); //time after which SDL was initialized
//
void Game::gameLoop() //loops each frame
{
	Graphics graphics;
	Input input;
	SDL_Event event; //stores data about any events happening in game
	//MouseControl mouse;


	this->_level = Level("city", Vector(100, 100), graphics);
	this->_player = Player(graphics, this->_level.getPlayerSpawnPoint());
	this->_hudHealth = HUDElements(graphics, "fonts/OpenSansBold.ttf", 24, 255, 0, 0, "health: ", globals::SCREEN_WIDTH * 0.01, globals::SCREEN_HEIGHT * 0.05, 100, 50);
	this->_hudScore = HUDElements(graphics, "fonts/OpenSansBold.ttf", 24, 0, 255, 0, "score: ", globals::SCREEN_WIDTH * 0.85, globals::SCREEN_HEIGHT * 0.05, 100, 50);
	this->_mouse = MouseControl();
	
	int enemyCount = 0;
	//const char * text = "200";
	//this->_hud._text + text;
	while (true) 
	{
		input.beginNewFrame();
		
		if (SDL_PollEvent(&event)) //if there's something happening
		{
			if (event.type == SDL_MOUSEBUTTONDOWN)
				this->_mouse.HandleEvent(&event, graphics, "assets/sprites/bullet1.png", this->_player.getX(), this->_player.getY(), 0.5, 100, 1, 1, 1, 1);
			else if (event.type == SDL_KEYDOWN) //if key is pressed
			{
				if (event.key.repeat == 0) //make sure it's not held down, we want a press of a key here
				{
					input.keyPressedEvent(event);
				}
			}
			else if (event.type == SDL_KEYUP) 
			{
					input.keyReleasedEvent(event);
			}
			else if (event.type == SDL_QUIT) 
			{
				return;
			}
		}
		if (input.wasKeyPressed(SDL_SCANCODE_ESCAPE)) //quit game on escape 
		{
			//add confirmation box
			return;
		} //no else if since we can move in two directions
		//reset movement
		this->_player._dx = 0;
		this->_player._dy = 0;
		//reset globals
		isMovingDown = false;
		isMovingRight = false;
		isMovingLeft = false;
		isMovingUp = false;

		//movement input 	
		if (input.isKeyHeld(SDL_SCANCODE_W)) 
		{
			this->_player.moveUp(isMovingRight, isMovingLeft, isMovingUp, isMovingDown);
		}

		if (input.isKeyHeld(SDL_SCANCODE_S)) 
		{
			this->_player.moveDown(isMovingRight, isMovingLeft, isMovingUp, isMovingDown);
		}

		if (input.isKeyHeld(SDL_SCANCODE_A))
		{
			this->_player.moveLeft(isMovingRight, isMovingLeft, isMovingUp, isMovingDown);
		}

		if (input.isKeyHeld(SDL_SCANCODE_D))
		{
			this->_player.moveRight(isMovingRight, isMovingLeft, isMovingUp, isMovingDown);
		}
		if (!input.isKeyHeld(SDL_SCANCODE_A) && !input.isKeyHeld(SDL_SCANCODE_S) && !input.isKeyHeld(SDL_SCANCODE_W) && !input.isKeyHeld(SDL_SCANCODE_D))
			this->_player.stopMoving();


		const int CURRENT_TIME_MS = SDL_GetTicks();
		int ELAPSED_TIME_MS = CURRENT_TIME_MS - LAST_UPDATE_TIME; //time it took for a frame to work
		this->update(std::min(ELAPSED_TIME_MS, MAX_FRAME_TIME)); //used to generalize execution time of physics in game independently from hardware
		LAST_UPDATE_TIME = CURRENT_TIME_MS; //update last update time with value of latest frame time

		if (enemyCount <= 6) 
		{
			//enemy spawn logic
			int randomSpawn = rand() % this->_level._enemySpawnPoints.size();
			Vector spawnCoordinates = this->_level._enemySpawnPoints.at(randomSpawn);
			//issues with push back, not visible to level 
			this->_level._enemies.push_back(new Armor1(graphics, this->_player.getX(), this->_player.getY(), spawnCoordinates));
			enemyCount++;
		}

		



		this->draw(graphics);

		std::cout << "X: " << this->_player.getX() << std::endl;
		std::cout << "Y: " << this->_player.getY() << std::endl << std::flush;
		//system("CLS");
	}
}

void Game::draw(Graphics & graphics)
{
	graphics.clear();
	this->_level.draw(graphics);
	this->_player.draw(graphics);
	for (int i = 0; i < this->_mouse._bullets.size(); i++) 
	{
		this->_mouse._bullets.at(i)->draw(graphics);
	}
	this->_hudHealth.draw(graphics);
	this->_hudScore.draw(graphics);
	graphics.flip();
}

void Game::update(float elapsedTime)
{
	this->_player.update(elapsedTime);
	this->_level.update(elapsedTime, this->_player);
	for (int i = 0; i < this->_mouse._bullets.size(); i++)
	{
		this->_mouse._bullets.at(i)->update(elapsedTime);
	}
	//check collisions
	std::vector<Rectangle> others;
	std::vector<Rectangle> othersEnemy;
	 
	//if this statement returns at least one
	if ((others = this->_level.checkTileCollisions(this->_player.getBoundingBox())).size() > 0)
	{
		//player collided with something
		this->_player.handleTileCollisions(others);
	}

	//this isnt working properly
	for (int i = 0; i < this->_level._enemies.size(); i++)
	{
		//check if enemy is hit
		if ((othersEnemy = this->_mouse.checkBulletCollisions(this->_level._enemies.at(i)->getBoundingBox())).size() > 0)
		{
			//enemy collided with bullet
			this->_level._enemies.at(i)->handleBulletCollisions(othersEnemy, this->_mouse._bullets.at(i));
		}
		othersEnemy.clear();
		//check if enemy hits a wall
		if ((othersEnemy = this->_level.checkTileCollisions(this->_level._enemies.at(i)->getBoundingBox())).size() > 0)
		{
			//enemy collided with something
			this->_level._enemies.at(i)->handleTileCollisions(othersEnemy);
		}
	}
	
	
	
	
	
}


