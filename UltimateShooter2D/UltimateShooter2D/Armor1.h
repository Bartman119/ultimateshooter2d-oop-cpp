#pragma once
#ifndef ARMOR1_H
#define ARMOR1_H
#include "Enemy.h"

/**
constants of armor1 class
*/
namespace armor1_constants
{
	const float WALK_SPEED = 0.2f; /**< const value showing speed of enemy */
}

/**
Class representing parameters of Armor1 enemy in game
*/
class Armor1 : public Enemy 
{
private:
	
public:
	float posX; /**< x position of enemy */
	float posY; /**< y position of enemy */
	
	/** 
		Default constructor being removed
	*/
	Armor1() = delete; 
	
	/**
		Constructor passing parameters to Enemy class it derives from
		@param graphics Graphics class member representing SDL graphical functionality
		@param playerX current player's Xcoordinate
		@param playerY current player's Ycoordinate
		@param spawnPoint pair of coordinates that represent a spawn point for the enemy
	*/
	Armor1(Graphics &graphics, float playerX, float playerY, Vector spawnPoint);
	
	/**
		function updating enemy
		@param elapsedTime value representing time, used to unify time it takes to make operations on different PCs
		@param player Player class passed to get its parameters
	*/
	void update(int elapsedTime, Player &player);
	
	/**
	function that draws enemy periodically
	@param graphics Graphics class member representing SDL graphical functionality
	*/
	void draw(Graphics &graphics);

	/**
		function virtually passed from animated sprite
		@param currentAnimation name of current animation
	*/
	void animationDone(std::string currentAnimation);
	
	/**
		function virtually passed from animated sprite
	*/
	void setupAnimations();

	/**
		getter for Xcoordinate
	*/
	float getPosX() { return posX; };
	
	/**
		getter for Ycoordinate
	*/
	float getPosY() { return posY; };

	/**
		getter for speed of enemy
	*/
	const float getSpeed() { return armor1_constants::WALK_SPEED; };
};


#endif