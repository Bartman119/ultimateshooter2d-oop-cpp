#include "AnimatedSprite.h"
#include "Graphics.h"

void AnimatedSprite::addAnimation(int numberOfFrames, int x, int y, std::string name, int width, int height, Vector offset)
{
	std::vector<SDL_Rect> rectangles; //temporary storage of frames
	for (int i = 0; i < numberOfFrames; i++) {
		SDL_Rect newRect = { (i + x) * width, y, width, height };
		rectangles.push_back(newRect);
	}
	this->_animations.insert(std::pair<std::string, std::vector<SDL_Rect>>(name, rectangles));
	this->_offsets.insert(std::pair<std::string, Vector>(name, offset));
}

void AnimatedSprite::resetAnimations()
{
	this->_animations.clear();
	this->_offsets.clear();
}

void AnimatedSprite::stopAnimation()
{
	this->_frameIndex = 0;
	this->animationDone(this->_currentAnimation);
}

void AnimatedSprite::setVisible(bool isVisible)
{
	this->_isVisible = isVisible;
}



AnimatedSprite::AnimatedSprite() {}

AnimatedSprite::AnimatedSprite(Graphics & graphics, const std::string & filePath, int sourceX, int sourceY,
	int width, int height, float posX, float posY, float timeToUpdate):
	Sprite(graphics, filePath, sourceX, sourceY, width, height, posX, posY), _frameIndex(0), _timeToUpdate(timeToUpdate), _isVisible(true), _currentAnimationOnce(false), _currentAnimation(""), _timeElapsed(0)
{

}

void AnimatedSprite::playAnimation(std::string animation, bool once)
{
	this->_currentAnimationOnce = once;
	if (this->_currentAnimation != animation) 
	{
		this->_currentAnimation = animation;
		this->_frameIndex = 0;
	}


}

void AnimatedSprite::update(int elapsedTime)
{
	Sprite::update();

	this->_timeElapsed += elapsedTime;
	if (this->_timeElapsed > this->_timeToUpdate) 
	{
		this->_timeElapsed -= this->_timeToUpdate;
		//check if frame is not the last frame in animation
		if (this->_frameIndex < this->_animations[this->_currentAnimation].size() - 1) 
		{
			this->_frameIndex++;
		}
		else 
		{
			//our animation is said to play once
			if (this->_currentAnimationOnce == true) 
			{
				this->setVisible(false);
			}
			this->stopAnimation();
		}
	}
}

void AnimatedSprite::draw(Graphics & graphics, int x, int y)
{
	if (this->_isVisible) 
	{
		SDL_Rect destinationRectangle;
		//add coordinate value of given animation to current value
		destinationRectangle.x = x + this->_offsets[this->_currentAnimation].x;
		destinationRectangle.y = y + this->_offsets[this->_currentAnimation].y;
		destinationRectangle.w = this->_sourceRect.w * globals::SPRITE_SCALE;
		destinationRectangle.h = this->_sourceRect.h * globals::SPRITE_SCALE;
		
		SDL_Rect sourceRect = this->_animations[this->_currentAnimation][this->_frameIndex];
		graphics.blitSurface(this->_spriteSheet, &sourceRect, &destinationRectangle);
	}
}

