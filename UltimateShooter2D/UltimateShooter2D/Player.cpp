#include "Player.h"
#include "Graphics.h"
#include "Globals.h"

namespace player_constants 
{
	const float WALK_SPEED = 0.3f;
}


Player::Player(){}

Player::Player(Graphics & graphics, Vector spawnPoint) : AnimatedSprite(graphics, "assets/sprites/Character 1/Walk/Character1Walk.png", 0, 0, 40, 40, spawnPoint.x, spawnPoint.y, 100),
_dx(0), _dy(0), _facing(DOWN), _spawnPointX(spawnPoint.x), _spawnPointY(spawnPoint.y)
{
	graphics.loadImage("assets/sprites/Character_1/Walk/Character1Walk.png");
	this->setupAnimations();
	this->playAnimation("IdleRight");
}

void Player::draw(Graphics & graphics)
{
	AnimatedSprite::draw(graphics, this->getX()/*_spawnPointX*/, this->getY()/*_spawnPointY*/);
}

void Player::update(float elapsedTime)
{
	//Move by dx
	this->_x += this->_dx * elapsedTime; //works better with new functions!
	//Move by dy
	this->_y += this->_dy * elapsedTime;

	AnimatedSprite::update(elapsedTime);
}

void Player::setupAnimations() {
	this->addAnimation(8, 1, 0, "RunUp", 64, 64, Vector());
	this->addAnimation(8, 1, 64, "RunLeft", 64, 64, Vector());
	this->addAnimation(8, 1, 128, "RunDown", 64, 64, Vector());
	this->addAnimation(8, 1, 192, "RunRight", 64, 64, Vector());
	this->addAnimation(1, 0, 0, "IdleUp", 64, 64, Vector());
	this->addAnimation(1, 0, 64, "IdleLeft", 64, 64, Vector());
	this->addAnimation(1, 0, 128, "IdleDown", 64, 64, Vector());
	this->addAnimation(1, 0, 192, "IdleRight", 64, 64, Vector());
}

//handles all collisions where the player is colliding with tiles 
void Player::handleTileCollisions(std::vector<Rectangle>& others)
{
	//figure out what side collision happened on and move accordingly
	for (int i = 0; i < others.size(); i++) 
	{
		sides::Side collisionSide = Sprite::getCollisionSide(others.at(i));
		if (collisionSide != sides::NONE) 
		{
			switch(collisionSide)
			{
			case sides::TOP:
				//set top of sprite to bottom of object + 1 pixel
				this->_y = others.at(i).getBottom() + 1;
				this->_dy = 0;
				break;
			case sides::BOTTOM:
				this->_y = others.at(i).getTop() - this->_boundingBox.getHeight() - 1;
				this->_dy = 0;
				break;
			case sides::LEFT:
				this->_x = others.at(i).getRight() + 1;
				break;
			case sides::RIGHT:
				this->_x = others.at(i).getLeft() - this->_boundingBox.getWidth() - 1;
				break;
			}
		}
	}
}

void Player::moveUp(bool & isMovingRight, bool & isMovingLeft, bool & isMovingUp, bool & isMovingDown)
{
	this->_dy = -player_constants::WALK_SPEED;
	if (!isMovingDown && !isMovingLeft && !isMovingRight)
		this->playAnimation("RunUp");
	this->_facing = UP;
	isMovingUp = true;
}

void Player::moveDown(bool & isMovingRight, bool & isMovingLeft, bool & isMovingUp, bool & isMovingDown)
{
	this->_dy = player_constants::WALK_SPEED;
	if (!isMovingUp && !isMovingLeft && !isMovingRight)
		this->playAnimation("RunDown");
	this->_facing = DOWN;
	isMovingDown = true;
}

void Player::moveLeft(bool & isMovingRight, bool & isMovingLeft, bool & isMovingUp, bool & isMovingDown)
{
	/*if (this->_dy != 0)
	{
		this->_dy *= 0.75;
		this->_dx = -player_constants::WALK_SPEED * 0.75;
	}
	else*/
	this->_dx = -player_constants::WALK_SPEED;
	if (!isMovingDown && !isMovingUp && !isMovingRight)
		this->playAnimation("RunLeft");
	this->_facing = LEFT;
	isMovingLeft = true;
}

void Player::moveRight(bool & isMovingRight, bool & isMovingLeft, bool & isMovingUp, bool & isMovingDown)
{
	/*if (this->_dy != 0)
	{
		this->_dy *= 0.75;
		this->_dx = player_constants::WALK_SPEED * 0.75;
	}
	else*/
	this->_dx = player_constants::WALK_SPEED;
	if (!isMovingDown && !isMovingLeft && !isMovingUp)
		this->playAnimation("RunRight");
	this->_facing = RIGHT;
	isMovingRight = true;
}

void Player::stopMoving()
{
	this->_dx = 0.0f;
	this->_dy = 0.0f;
	if (_facing == RIGHT)
		this->playAnimation("IdleRight");
	else if (_facing == LEFT)
		this->playAnimation("IdleLeft");
	else if (_facing == UP)
		this->playAnimation("IdleUp");
	else
		this->playAnimation("IdleDown");
}

const float Player::getX() const
{
	return this->_x;
}

const float Player::getY() const
{
	return this->_y;
}

void Player::animationDone(std::string currentAnimation) {};

