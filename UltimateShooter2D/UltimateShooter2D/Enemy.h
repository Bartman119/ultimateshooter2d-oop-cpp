#pragma once
#ifndef ENEMY_H
#define ENEMY_H


#include "AnimatedSprite.h"
#include "Player.h"
#include "Globals.h"
#include "Bullet.h"

#include <string>

/**
	Main class for enemy representation, deriving from AnimatedSprite
*/
class Enemy : public AnimatedSprite 
{
private:
	float _playerSpawnX; /**< xCoordinate of player spawn point*/
	float _playerSpawnY; /**< yCoordinate of player spawn point*/
	float _enemyPosX; /**< xCoordinate of enemy position*/
	float _enemyPosY; /**< yCoordinate of enemy position*/
protected:
	float _playerPosX; /**< xCoordinate of current player position*/
	float _playerPosY; /**< yCoordinate of current player position*/
	Direction _direction; /**< Direction structure representing which way creature is moving*/
	int _maxHealth = 300; /**< maximal health value for enemies */
	int _currentHealth = 300; /**< starting health value of enemies*/

public:
	Enemy() = delete;
	
	/**
	Constructor passing enemies parameters to AnimatedSprite and fills member with values 
	@param graphics Graphics class member representing SDL graphical functionality
	@param playerX current Xcoordinate of player
	@param playerY current Ycoordinate of player
	@param enemyPosX x coordinate of enemy
	@param enemyPosY y coordinate of enemy
	@param filePath path to file with sprites for enemy
	@param sourceX starting X coordinate of an animation
	@param sourceY starting Y coordinate of an animation
	@param width pixel value of width of a single frame
	@param height pixel value of height of a single frame
	@param posX current X coordinate position of an object that is drawn in game
	@param posY current Y coordinate position of an object that is drawn in game
	@param spawnPoint pair of coordinates to spawn place for enemy
	@param timeToUpdate value generalizing time it takes for a pc to make an operation
	*/
	Enemy(Graphics & graphics, float playerX, float playerY, float enemyPosX, float enemyPosY, std::string filePath, int sourceX, int sourceY, int width, int height, Vector spawnPoint, int timeToUpdate);
	
	/**
	simple destructor for enemy
	*/
	~Enemy();

	/**
		function updating class
		@param elapsedTime value representing time, used to unify time it takes to make operations on different PCs
		@param Player class for data
		@param enemyPosX x coordinate of enemy
		@param enemyPosY y coordinate of enemy
	*/
	virtual void update(int elapsedTime, Player &player, float enemyPosX, float enemyPosY);
	
	/**
	function that draws animations periodically
	@param graphics Graphics class member representing SDL graphical functionality
	*/
	virtual void draw(Graphics &graphics);

	/**
	virtual function passed form AnimatedSprite for finishing up animation
	@param currentAnimation name of current animation
	*/
	void animationDone(std::string currentAnimation);
	
	/**
	virtual function for setting animation up
	*/
	void setupAnimations();

	/**
	function handling collisions between enemies and map 
	@param others all collidable tiles
	*/
	void handleTileCollisions(std::vector<Rectangle>& others);
	
	/**
	function handling collisions between enemies and bullets
	@param others all collidable bullets
	@param _bullet bullet class for data
	*/
	void handleBulletCollisions(std::vector<Rectangle>& others, Bullet* _bullet);
	
	
	/**
	getter for max health
	*/
	const inline int getMaxHealth() const { return this->_maxHealth; }
	
	/**
	getter for current health
	*/
	const inline int getCurrentHealth() const { return this->_currentHealth; }
	
	/**
	setter for current health
	@param health health change parameter
	*/
	void setCurrentHealth(int health) { this->_currentHealth -= health; }
	
	/**
	euclidean distance calculator
	@param enemyPosX X coordinate of enemy
	@param enemyPosY Y coordinate od enemy
	*/
	float calculateDistance(float enemyPosX, float enemyPosY);
	
	/**
	getter for enemy movement sspeed
	*/
	virtual const float getSpeed();
	
	/**
	getter for enemy position x coordinate
	*/
	float getPosX() { return _enemyPosX; };
	
	/**
	getter for enemy position Y coordinate
	*/
	float getPosY() { return _enemyPosY; };
};

#endif