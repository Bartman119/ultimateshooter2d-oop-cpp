#include "Input.h"

//Resets irrelevant keys at the beginning of each frame
void Input::beginNewFrame()
{
	//only clear definitive states of keys, held keys can be of multi-frame duration
	this->_pressedKeys.clear();
	this->_releasedKeys.clear();
}

//Called when key is pressed
void Input::keyPressedEvent(SDL_Event & event)
{
	this->_pressedKeys[event.key.keysym.scancode] = true;
	this->_heldKeys[event.key.keysym.scancode] = true;
}

//Called when key is released
void Input::keyReleasedEvent(SDL_Event & event)
{
	this->_releasedKeys[event.key.keysym.scancode] = true;
	this->_heldKeys[event.key.keysym.scancode] = false;
}

//Check if certain key is pressed
bool Input::wasKeyPressed(SDL_Scancode key)
{
	return this->_pressedKeys[key];
}

//Check if certain key is released
bool Input::wasKeyReleased(SDL_Scancode key)
{
	return this->_releasedKeys[key];
}

//Check if certain key is held
bool Input::isKeyHeld(SDL_Scancode key)
{
	return this->_heldKeys[key];
}
